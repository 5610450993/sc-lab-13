package problem2;

import problem1.Person;

public class Consumer implements Runnable{
	private static final int DELAY = 1; 
	private Queue q;
	
	public Consumer(Queue q){
		this.q = q;
	}
	
	@Override
	public void run() {
		try {
			q.dequeue();
			Thread.sleep(DELAY);
			
		} catch (InterruptedException e) {
			//e.printStackTrace();
		}
		
	}

}
