package problem2;

import problem1.Person;

public class Producer implements Runnable{
	private static final int DELAY = 1; 
	private Queue q;
	
	public Producer(Queue q){
		this.q = q;
	}
	
	@Override
	public void run() {
		try {
			q.enqueue();
			Thread.sleep(DELAY);
			
		} catch (InterruptedException e) {
			//e.printStackTrace();
		}
		
	}

}
