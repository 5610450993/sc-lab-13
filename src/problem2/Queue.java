package problem2;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Queue {
	private LinkedList<String>  queue;
	private Lock emptyQLock;
	private Condition sufficientFundsCondition;
	
	public Queue(){
		queue = new LinkedList<String>();
		emptyQLock = new ReentrantLock();
		sufficientFundsCondition = emptyQLock.newCondition();
	}

	
	public void dequeue(){
		emptyQLock.lock();
		try{
			sufficientFundsCondition.signalAll();
		}
		finally{
			emptyQLock.unlock();
		}
	}

	public void enqueue() throws InterruptedException{
		emptyQLock.lock();
		try{
			
		}
		finally{
			emptyQLock.unlock();
		}
	}
	
	public LinkedList<String> getList(){
		return queue;
	}
}
