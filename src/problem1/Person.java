package problem1;
import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Person {
	private LinkedList<String> person;
	private Lock personChangeLock;
	private Condition sufficientFundsCondition;
	
	public Person(){
		person = new LinkedList<String>();
		personChangeLock = new ReentrantLock();
		sufficientFundsCondition = personChangeLock.newCondition();
	}

	
	public void add(String name){
		personChangeLock.lock();
		try{
			System.out.print("ADD: "+ name);
			person.add(name);
			System.out.println(", new name is " + getList());
			sufficientFundsCondition.signalAll();
		}
		finally{
			personChangeLock.unlock();
		}
	}

	public void remove(String name) throws InterruptedException{
		personChangeLock.lock();
		try{
			System.out.print("REMOVE: "+ name);
			person.remove(name);
			System.out.println(", new name is " + getList());
		}
		finally{
			personChangeLock.unlock();
		}
	}
	
	public LinkedList<String> getList(){
		return person;
	}
}
