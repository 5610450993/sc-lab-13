package problem1;


public class PersonThreadRunner {

	public static void main(String[] args) {
		Person per = new Person();
		final String[] ADDNAME = {"Thanawan","Natachai","Sirada","Peeraya","Naru"};
		final String[] RENAME = {"Nattapon","Wiriya","Natachai","Chatachai","Sirada"};
	    final int THREADS = 5;

	    for (int i = 0; i < THREADS; i++){
	        AddRunnable a = new AddRunnable(per, ADDNAME[i]);
	        RemoveRunnable r = new RemoveRunnable(per, RENAME[i]);
	         
	        Thread at = new Thread(a);
	        Thread rt = new Thread(r);
	         
	        at.start();
	        rt.start();
	   }
	    

	}

}
