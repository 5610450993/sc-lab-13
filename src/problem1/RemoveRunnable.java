package problem1;
import java.util.LinkedList;


public class RemoveRunnable implements Runnable{
	private static final int DELAY = 1; 
	private Person per;
	private String name;
	
	public RemoveRunnable(Person per, String name){
		this.per = per;
		this.name = name;
	}
	
	@Override
	public void run() {
		try {
			per.remove(name);
			Thread.sleep(DELAY);
			
		} catch (InterruptedException e) {
			//e.printStackTrace();
		}
		
	}

}
