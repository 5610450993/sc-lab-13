package problem1;
import java.util.LinkedList;


public class AddRunnable implements Runnable{
	private static final int DELAY = 1; 
	private Person per;
	private String name;
	
	public AddRunnable(Person per, String name){
		this.per = per;
		this.name = name;
	}
	
	@Override
	public void run() {
		try {
			per.add(name);
			Thread.sleep(DELAY);
			
		} catch (InterruptedException e) {
			//e.printStackTrace();
		}
		
	}

}
